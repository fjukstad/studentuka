//
//  ProgramDetailViewController.m
//  Studentuka 2013
//
//  Created by Bjørn Fjukstad on 12/31/12.
//  Copyright (c) 2012 studentuka. All rights reserved.
//

#import "ProgramDetailViewController.h"
// For social integration
#import <Social/Social.h>


@interface ProgramDetailViewController ()

@end

@implementation ProgramDetailViewController
@synthesize event;
@synthesize browser;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // custom init
    }
    return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Fetch info from JSON
    NSString *title = [self.event objectForKey:@"event_name"];
    NSString *content = [self.event objectForKey:@"post_content"];
    
    // Fix url @ studentuka. JSON doesn't hand it over :(
    NSString *urlTitle = [self.event objectForKey:@"event_slug"];
    NSMutableString *fixedURL = [[NSMutableString alloc] initWithString:@"http://studentuka.no/events/"];
    [fixedURL appendString:urlTitle];
    
    // Path, so that we can use some neat css
    NSString *path = [[NSBundle mainBundle] bundlePath];
    NSURL *baseURL = [NSURL fileURLWithPath:path];
    
    // Formatting date and time
    NSString *date = [self.event objectForKey:@"start_date"];
    NSString *time = [self.event objectForKey:@"start_time"];

    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    
    // First date
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSDate* dateForm = [dateFormatter dateFromString:date];
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"nb_NO"]];
    [dateFormatter setDateFormat:@"EEEE d. MMMM "];
    NSString *startDate =[dateFormatter stringFromDate:dateForm];
    
    // Time
    [dateFormatter setDateFormat:@"HH:mm:ss"];
     dateForm = [dateFormatter dateFromString:time];
    [dateFormatter setDateFormat:@"HH:mm"];
    NSString *startTime = [dateFormatter stringFromDate:dateForm];

    
    
    // Creating the HTML page by hand
    NSString *htmlHead = @"<head><title>Title</title><link rel=\"stylesheet\" type=\"text/css\" href=\"style.css\" /></head><body>";
    //NSURL *url = [[NSURL alloc]initWithString:fixedURL];
    NSString *headerTitle = [NSString stringWithFormat:@"<header>%@</header>", title];
    NSString *postHtml = [NSString stringWithFormat:@"<post>%@</post>", content];
    NSString *dateHtml = [NSString stringWithFormat:@"<p class=\"author\">%@ klokken %@</p>", startDate, startTime];
    NSString *endBody = @"</body>";
    NSMutableString *body = [[NSMutableString alloc] initWithString:htmlHead];
    [body appendString:headerTitle];
    [body appendString:dateHtml];
    [body appendString:postHtml];
        
    
    [body appendString:endBody];
    
    
    NSLog(@"HTML: %@", body);

    [self.browser loadHTMLString:body baseURL:baseURL];
    
    // Set title to top bar
    [self setTitle:title];
    
    
    // Bring back the navbar
    self.navigationController.navigationBar.hidden = NO;
    
    
}

// Pop-up menu with the following options: åpne i safari, del på facebook, del på twitter
- (IBAction)showActions:(id)sender{
    UIActionSheet *popupQuery = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Avbryt" destructiveButtonTitle:nil otherButtonTitles:@"Åpne i Safari", @"Del på Facebook", @"Del på Twitter",  nil];
    
    popupQuery.actionSheetStyle = UIActionSheetStyleBlackOpaque;
    [popupQuery showInView:[self.view window]];
    [popupQuery release];
    
}

// Handling user input on action
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    // Fix url @ studentuka. JSON doesn't hand it over :(
    NSString *urlTitle = [self.event objectForKey:@"event_slug"];
    NSMutableString *fixedURL = [[NSMutableString alloc] initWithString:@"http://studentuka.no/events/"];
    [fixedURL appendString:urlTitle];
    
    // The message to be posted on facebook/twitter
    NSMutableString *message = [[NSMutableString alloc] initWithString:[self.event objectForKey:@"post_title"]];
    
    switch (buttonIndex) {
            // Safari
        case 0:
        {
            // Fetch URL from downloaded JSON and open Safari with that url
            NSURL *url = [[NSURL alloc]initWithString:fixedURL];
            if (![[UIApplication sharedApplication] openURL:url])
                NSLog(@"%@%@",@"Failed to open url:",[url description]);
            break;
        }
            // Facebook
        case 1:
        {
            
            if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook])
            {
                SLComposeViewController *faceSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
                
                [faceSheet setInitialText:message];
                [faceSheet addURL:[NSURL URLWithString:fixedURL]];
                //[faceSheet addImage:[UIImage imageNamed:@"barefugl114x114.png"]];
                
                [self presentViewController:faceSheet animated:YES completion:nil];
            }
            break;
        }
            // Twitter
        case 2:
        {
            // Twitter is set up and ready to go. If not iOS will notify user
            if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
            {
                SLComposeViewController *tweetSheet = [SLComposeViewController
                                                       composeViewControllerForServiceType:SLServiceTypeTwitter];
                [message appendString:@" - @StudentUKA2013"];
                // [tweet appendString:urlString];
                
                [tweetSheet setInitialText:message];
                [tweetSheet addURL:[NSURL URLWithString:fixedURL]];
                
                [self presentViewController:tweetSheet animated:YES completion:nil];
            }
            break;
        }
    }
}




- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
