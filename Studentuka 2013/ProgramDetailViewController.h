//
//  ProgramDetailViewController.h
//  Studentuka 2013
//
//  Created by Bjørn Fjukstad on 12/31/12.
//  Copyright (c) 2012 studentuka. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ProgramDetailViewController : UIViewController <UIWebViewDelegate, UIActionSheetDelegate>  {

    NSDictionary *event;
    IBOutlet UIWebView *browser;
    IBOutlet UIBarButtonItem *button;
}

@property (nonatomic, retain) UIWebView *browser;
@property (nonatomic, retain) NSDictionary *event;
- (IBAction)showActions:(id)sender;

@end
