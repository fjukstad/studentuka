//
//  SecondViewController.h
//  Studentuka 2013
//
//  Created by Bjørn Fjukstad on 12/9/12.
//  Copyright (c) 2012 studentuka. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PullToRefreshView.h"

@interface SecondViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, PullToRefreshViewDelegate> {
    
    IBOutlet UITableViewCell* output;
    IBOutlet UITableView *calendarView;
    
    NSArray *calendarFeed;
    int numRetries;
    
    NSMutableDictionary *sections;
    NSArray *sortedDays;
    
    NSDateFormatter *sectionDateFormatter;
    NSDateFormatter *timeFormatter;
    
    NSArray *events;
    
    UIActivityIndicatorView *spinner;
}
- (void) loadData;

@property (strong, nonatomic) NSDateFormatter *sectionDateFormatter;
@property (strong, nonatomic) NSDateFormatter *timeFormatter;
@property (strong, nonatomic) NSMutableDictionary *sections;
@property (strong, nonatomic) NSArray *sortedDays;
@property (nonatomic, retain) NSArray *events; 
@property (nonatomic, retain) NSArray *calendarFeed;
@property (nonatomic, retain) IBOutlet UITableView *calendarView;
@property (nonatomic, retain) IBOutlet UIActivityIndicatorView *spinner;

@end
