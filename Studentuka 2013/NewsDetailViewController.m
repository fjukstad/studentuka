//
//  NewsDetailViewController.m
//  Studentuka 2013
//
//  Created by Bjørn Fjukstad on 12/11/12.
//  Copyright (c) 2012 studentuka. All rights reserved.
//

#import "NewsDetailViewController.h"

// For social integration
#import <Social/Social.h>


@interface NewsDetailViewController ()

@end

@implementation NewsDetailViewController
@synthesize browser;
@synthesize newsStory; 

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // custom init
    }
    return self;
}
-(id) initWithDict:(NSDictionary *) dict{
    // call the base class ini
    if (!(self=[super init]))
        return nil;
    
    // set your retained property
    // self->newsStory = dict;
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Fetch from JSON
    NSString* post = [self.newsStory objectForKey:@"content"];
    //NSNumber* postid = [self.newsStory objectForKey:@"id"];
    NSString* title = [self.newsStory objectForKey:@"title"];
    NSString* date = [self.newsStory objectForKey:@"date"];
    //NSString* urlString = [self.newsStory objectForKey:@"url"];
    
    // Date formatting
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate* dateForm = [dateFormatter dateFromString:date];
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"nb_NO"]];
    [dateFormatter setDateFormat:@"d. MMMM yyyy"];
    NSString *formattedDate =[dateFormatter stringFromDate:dateForm];

    
    // Path, so that we can use some neat css
    NSString *path = [[NSBundle mainBundle] bundlePath];
    NSURL *baseURL = [NSURL fileURLWithPath:path];
    
    NSString *htmlHead = @"<head><title>CSS Example</title><link rel=\"stylesheet\" type=\"text/css\" href=\"style.css\" /></head><body>";

    
    //NSURL *url = [[NSURL alloc]initWithString:urlString];
    NSString *headerTitle = [NSString stringWithFormat:@"<header>%@</header><META name=\"viewport\" content=\"width=device-width\" />", title];
    NSString *dateHtml = [NSString stringWithFormat:@"<p class=\"author\">den %@</p>", formattedDate];
    NSString *postHtml = [NSString stringWithFormat:@"<post>%@</post>", post];
    NSString *endBody = @"</body>";
    
    NSMutableString *body = [[NSMutableString alloc] initWithString:htmlHead];
    
    [body appendString:headerTitle];
    [body appendString:dateHtml];
    [body appendString:postHtml];
    
    [body appendString:endBody];

    [self.browser loadHTMLString:body baseURL:baseURL];
    
    
    [self setTitle:title];
    
    // Bring back the navbar
    self.navigationController.navigationBar.hidden = NO;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)setTitle:(NSString *)title
{
    [super setTitle:title];
    UILabel *titleView = (UILabel *)self.navigationItem.titleView;
    if (!titleView) {
        titleView = [[UILabel alloc] initWithFrame:CGRectZero];
        titleView.backgroundColor = [UIColor clearColor];
        titleView.font = [UIFont boldSystemFontOfSize:17.0];
        titleView.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
        
        titleView.textColor = [UIColor whiteColor]; // Change to desired color
        
        self.navigationItem.titleView = titleView;
        [titleView release];
    }
    titleView.text = title;
    [titleView sizeToFit];
}

// Pop-up menu with the following options: åpne i safari, del på facebook, del på twitter
- (IBAction)showActions:(id)sender{
    UIActionSheet *popupQuery = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Avbryt" destructiveButtonTitle:nil otherButtonTitles:@"Åpne i Safari", @"Del på Facebook", @"Del på Twitter",  nil];
    
    popupQuery.actionSheetStyle = UIActionSheetStyleBlackOpaque;
    [popupQuery showInView:[self.view window]];
    [popupQuery release];

}

// Handling user input on action 
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    // Get URL of news story
    NSString *urlString = [self.newsStory objectForKey:@"url"];
    NSMutableString *message = [[NSMutableString alloc] initWithString:[self.newsStory objectForKey:@"title"]];
    
    switch (buttonIndex) {
        // Safari
        case 0:
        {
            // Fetch URL from downloaded JSON and open Safari with that url
            NSString *urlString = [self.newsStory objectForKey:@"url"];
            NSURL *url = [[NSURL alloc]initWithString:urlString];
            if (![[UIApplication sharedApplication] openURL:url])
                NSLog(@"%@%@",@"Failed to open url:",[url description]);
            break;
        }
        // Facebook
        case 1:
        {
            
            if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook])
            {
                SLComposeViewController *faceSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];

                [faceSheet setInitialText:message];
                [faceSheet addURL:[NSURL URLWithString:urlString]];
                //[faceSheet addImage:[UIImage imageNamed:@"barefugl114x114.png"]];

                [self presentViewController:faceSheet animated:YES completion:nil];
            }
            break;
        }
        // Twitter
        case 2:
        {            
            // Twitter is set up and ready to go. If not iOS will notify user
            if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
            {
                SLComposeViewController *tweetSheet = [SLComposeViewController
                                                       composeViewControllerForServiceType:SLServiceTypeTwitter];
                [message appendString:@" - @StudentUKA2013"];
                // [tweet appendString:urlString];
                
                [tweetSheet setInitialText:message];
                [tweetSheet addURL:[NSURL URLWithString:urlString]];

                [self presentViewController:tweetSheet animated:YES completion:nil];
            }
            break;
        }
    }
}


@end
