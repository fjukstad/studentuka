//
//  FirstViewController.h
//  Studentuka 2013
//
//  Created by Bjørn Fjukstad on 12/9/12.
//  Copyright (c) 2012 studentuka. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PullToRefreshView.h"

@interface FirstViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, UIAlertViewDelegate, PullToRefreshViewDelegate>{

    IBOutlet UITableViewCell* output;
    IBOutlet UITableView *newsTable;
    IBOutlet UIImageView *imageViewer; 
    NSArray *newsFeed;
    int numRetries;
    
    UIActivityIndicatorView *spinner; 

}
- (void) loadData;

@property (nonatomic, retain) NSArray *newsFeed;
@property (nonatomic, retain) IBOutlet UITableView *newsTable;
@property (nonatomic, retain) IBOutlet UIActivityIndicatorView *spinner;

@end
