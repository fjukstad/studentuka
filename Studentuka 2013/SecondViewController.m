//
//  SecondViewController.m
//  Studentuka 2013
//
//  Created by Bjørn Fjukstad on 12/9/12.
//  Copyright (c) 2012 studentuka. All rights reserved.
//

#import "SecondViewController.h"
#import "ProgramDetailViewController.h" 

// Nice gradient
#import <QuartzCore/QuartzCore.h>

#define queue dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)
#define calendarUrl [NSURL URLWithString:@"http://studentuka.net/api/get_recent_events/"]

@interface SecondViewController ()

@end

@implementation SecondViewController
{
    
    PullToRefreshView *pull;
   
}

@synthesize calendarView;
@synthesize calendarFeed;
@synthesize spinner; 
@synthesize sections;
@synthesize sortedDays;
@synthesize sectionDateFormatter;
@synthesize timeFormatter;
@synthesize events;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Grey stuff on top!!
    CGRect frame = self.view.bounds;
    frame.origin.y = -frame.size.height;
    UIView* grayView = [[UIView alloc] initWithFrame:frame];
    CGFloat nRed=51.0/255.0;
    CGFloat nBlue=53.0/255.0;
    CGFloat nGreen=59.0/255.0;
    UIColor *grey=[[UIColor alloc]initWithRed:nRed green:nBlue blue:nGreen alpha:1.0];
    grayView.backgroundColor = grey;
    [self.calendarView addSubview:grayView];
    [grayView release];
    
    
    pull = [[PullToRefreshView alloc] initWithScrollView:(UIScrollView *) self.calendarView];
    [pull setDelegate:self];
    [self.calendarView addSubview:pull];
    
    [self loadData];
    
}
- (void)pullToRefreshViewShouldRefresh:(PullToRefreshView *)view;
{
    [self loadData];
    [pull finishedLoading];
}

- (void) loadData{
    
    // Spinner for indicating work
    self.spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    self.spinner.center = self.view.center;
    self.spinner.hidesWhenStopped = YES;
    [self.view addSubview:spinner];
    self.spinner.transform = CGAffineTransformMakeScale(1.25f, 1.25f);
    [self.spinner startAnimating];

    // Fetch some news from Studentuka!
    dispatch_async(queue, ^{
        
        // Only ok if performed in a background thread
        NSData* data = [NSData dataWithContentsOfURL:
                        calendarUrl];
        // When data has been downloaded, the method fetchedData is
        // called.
        [self performSelectorOnMainThread:@selector(fetchedData:)
                               withObject:data waitUntilDone:YES];
    });
    
}
- (void)fetchedData:(NSData *)responseData {
    if(responseData == nil){
        [self showMessage:self];
    }
    else{
        //parse out the json data
        NSError* error;
        NSDictionary* json = [NSJSONSerialization
                              JSONObjectWithData:responseData //1
                              
                              options:kNilOptions
                              error:&error];
        self.events = [json objectForKey:@"posts"];
        
        [self.spinner stopAnimating];
        
        
        self.sections = [NSMutableDictionary dictionary];

        // Create dicitionary with date -> indicies mapping
        // where indicies are events on the given date 
        for (int i = 0; i < [self.events count]; i++)
        {
            NSDictionary *event = [self.events objectAtIndex:i];
           // NSLog(@"Event: %@", event );
            NSString *startDate = [event objectForKey:@"start_date"];
            
            NSMutableArray *indicies = [[NSMutableArray alloc]initWithObjects:[[NSNumber alloc] initWithInt:i], nil];
            
            // Check if this entry exists in dict, if not search trough
            // events for other events with this date and insert it into the
            // dictionary
            if([self.sections valueForKey:startDate]){
                continue; 
            }
            else{
                // Search for other occurences of this date
                for(int j = i+1; j < [self.events count]; j++){
                    NSDictionary *otherEvent = [self.events objectAtIndex:j];
                    NSString *otherStart = [otherEvent objectForKey:@"start_date"];
                    // If dates match, add its intex to indicies
                    if([startDate isEqualToString:otherStart]){
                        [indicies addObject:[[NSNumber alloc]initWithInt:j]];
                    }
                }
                
                [self.sections setValue:indicies forKey:startDate];
            }
        }
        
        // Create list of all days    
        NSArray *unsortedDays = [self.sections allKeys];
        self.sortedDays = [unsortedDays sortedArrayUsingSelector:@selector(compare:)];
        
        // Now we got a mapping, lets get that shit on the screen!!
        
        
        self.sectionDateFormatter = [[NSDateFormatter alloc] init];
        [self.sectionDateFormatter setDateStyle:NSDateFormatterLongStyle];
        [self.sectionDateFormatter setTimeStyle:NSDateFormatterNoStyle];
        
        self.timeFormatter = [[NSDateFormatter alloc] init];
        
        // Force update of data in table
        [calendarView reloadData];
    }
}

// Number of sections, i.e. number of days with events. 
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return [self.sections count];
}

// Return number of rows for the given section
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionNumber{
    NSString *date =  [self.sortedDays objectAtIndex:sectionNumber];
    NSArray *indicies = [self.sections objectForKey:date];
    return [indicies count]; 
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)sectionNumber{
    
    NSString *date = [self.sortedDays objectAtIndex:sectionNumber];
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSDate* dateForm = [dateFormatter dateFromString:date];
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"nb_NO"]];
    [dateFormatter setDateFormat:@"EEEE d. MMMM "];
    return [dateFormatter stringFromDate:dateForm];
}

/* Making the navbar dissappear */
- (void)viewWillAppear:(BOOL)animated {
    self.navigationController.navigationBar.hidden = YES;
    

    
}

- (CGFloat)tableView:(UITableView *) tableView heightForRowAtIndexPath: (NSIndexPath *)indexPath    {
    /*
    // CGSize cellHeight;
    static NSString *cellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if(cell == nil){
        return 10;
    }
    UIFont *font = [UIFont systemFontOfSize:14.0f];
    NSString *text = cell.textLabel.text;
    CGSize withinSize = CGSizeMake(cell.contentView.bounds.size.width, FLT_MAX);
    CGSize size = [text sizeWithFont:font constrainedToSize:withinSize lineBreakMode:UILineBreakModeWordWrap];
    */
    int sectionNumber = indexPath.row;
    
    NSString *text = [self tableView:tableView titleForHeaderInSection:sectionNumber];
    UIFont *font = [UIFont fontWithName:@"Helvetica" size:17.0];
    //CGSize constraintSize = CGSizeMake(280.0f, MAXFLOAT);
    
    CGSize constraintSize = CGSizeMake(280.0f, MAXFLOAT);
    CGSize labelSize = [text sizeWithFont:font constrainedToSize:constraintSize lineBreakMode:NSLineBreakByTruncatingTail];
    
    return labelSize.height + 20;
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier] autorelease];
        
        cell.textLabel.numberOfLines = 0;
        cell.textLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        cell.textLabel.font = [UIFont fontWithName:@"Helvetica" size:17.0];
        //cell.textLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        
        CGRect screenRect = [[UIScreen mainScreen] bounds];
        CGFloat screenWidth = screenRect.size.width;
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(4.0f, 4.0f, screenWidth*0.8, 30.0f)];
        [cell.contentView addSubview:label];
        [label setTag:123];
        [label release];
        
        cell.detailTextLabel.font = [UIFont systemFontOfSize:14];
        cell.detailTextLabel.numberOfLines = 0;
        cell.detailTextLabel.lineBreakMode = NSLineBreakByCharWrapping;
        
        // Cell selection color 
        CGFloat nRed=219.0/255.0;
        CGFloat nBlue=176.0/255.0;
        CGFloat nGreen=0.0/255.0;
        // UIColor *yellow=[[UIColor alloc]initWithRed:nRed green:nBlue blue:nGreen alpha:1.0];
        nRed=55.0/255.0;
        nBlue=106.0/255.0;
        nGreen=188.0/255.0;
        UIColor *blue=[[UIColor alloc]initWithRed:nRed green:nBlue blue:nGreen alpha:1.0];
        
        
        UIView *v = [[[UIView alloc] init] autorelease];
    	v.backgroundColor = blue;
    	cell.selectedBackgroundView = v;
        
    }
    
    // Get date and its events
    NSString *date =  [self.sortedDays objectAtIndex:indexPath.section];
    NSArray *indicies = [self.sections objectForKey:date];
    
    // Which index in the indicies map we need
    NSUInteger index = (NSUInteger )[indexPath row];
    
    // get the value stored at this index in the indicies, so we can
    // use it to lookup in the JSON dump 
    NSValue *temp =  [indicies objectAtIndex:index];
    int indexInDump = 0;
    [temp getValue:&indexInDump];
    
    NSDictionary *event = [self.events objectAtIndex:indexInDump];
    
    NSString *title = [event objectForKey:@"post_title"];
    NSString *startTime = [event objectForKey:@"start_time"];
    // NSString *allDay = [event objectForKey:@"event_all_day"];
    
    [self.timeFormatter setDateFormat:@"HH:mm:ss"];
    NSDate* dateForm = [self.timeFormatter dateFromString:startTime];
    
    // If we need Nowrwegian formatting... 
    //[self.timeFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"nb_NO"]];
    
    [self.timeFormatter setDateFormat:@"HH:mm"];
    NSString *formattedStartTime = [self.timeFormatter stringFromDate:dateForm];
    
    // cell.textLabel.text = title;
    
    // Recover the label
    UILabel *labelView = (UILabel *)[cell viewWithTag:123];
    [labelView setText:title];

    cell.detailTextLabel.text = formattedStartTime;
    return cell;
}

-(UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    static NSString *CellIdentifier = @"sectionHeader";
    UITableViewCell *headerView = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (headerView == nil){
        [NSException raise:@"headerView == nil.." format:@"No cells with matching CellIdentifier loaded from your storyboard"];
    }
    
    UILabel *label = (UILabel *)[headerView viewWithTag:123];
    [label setText:@"penis"];
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = headerView.bounds;
    
    CGFloat nRed=55.0/255.0;
    CGFloat nBlue=106.0/255.0;
    CGFloat nGreen=188.0/255.0;
    UIColor *myColor=[[UIColor alloc]initWithRed:nRed green:nBlue blue:nGreen alpha:1.0];
    
    nRed=45.0/255.0;
    nBlue=96.0/255.0;
    nGreen=178.0/255.0;
    UIColor *myColor2=[[UIColor alloc]initWithRed:nRed green:nBlue blue:nGreen alpha:1.0];
    /*
     nRed=55.0/255.0;
     nBlue=106.0/255.0;
     nGreen=188.0/255.0;
    */
    gradient.colors = [NSArray arrayWithObjects:(id)[myColor CGColor],(id)[myColor2 CGColor], nil];
    [headerView.layer addSublayer:gradient];
    
    headerView.tag = section;
    headerView.textLabel.text = [NSString stringWithFormat:@"Section title %i", section];

    
    // create the label
	UILabel * headerLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, -3, 300, headerView.frame.size.height)];
	headerLabel.backgroundColor = [UIColor clearColor];
	headerLabel.opaque = NO;
	headerLabel.textColor = [UIColor whiteColor];
	headerLabel.highlightedTextColor = [UIColor whiteColor];
	headerLabel.font = [UIFont boldSystemFontOfSize:label.font.pointSize];
    headerLabel.text = [self tableView:tableView titleForHeaderInSection:section];
    
    //headerLabel.shadowColor  = [UIColor gColor];
    //headerLabel.shadowOffset = CGSizeMake(1.0, 0.0);
    
   // - (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)sectionNumber{

    /*CGRect sepFrame = CGRectMake(0, headerView.frame.size.height-1, 320, 1);
    UIView *seperatorView = [[[UIView alloc] initWithFrame:sepFrame] autorelease];
    seperatorView.backgroundColor = [UIColor colorWithWhite:224.0/255.0 alpha:1.0];
    [headerView addSubview:seperatorView];*/
    

                        
    // package and return
    [headerView addSubview:headerLabel];
    [headerLabel release];
    
    return headerView;
}
 

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self performSegueWithIdentifier:@"programDetail" sender:self];
    
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"programDetail"])
    {
        
        ProgramDetailViewController *viewController = (ProgramDetailViewController *)segue.destinationViewController;
        
        // Get index path selected
        NSIndexPath *indexPath = [calendarView indexPathForSelectedRow];
        
        // Fetch event from given index path
        NSString *date =  [self.sortedDays objectAtIndex:indexPath.section];
        NSArray *indicies = [self.sections objectForKey:date];
        NSUInteger index = (NSUInteger )[indexPath row];
        NSValue *temp =  [indicies objectAtIndex:index];
        int indexInDump = 0;
        [temp getValue:&indexInDump];
        
        // Set event, so we can access from new view controller
        viewController.event = [self.events objectAtIndex:indexInDump];

        // Deselect so that it isn't selected when we return
        [calendarView deselectRowAtIndexPath:[calendarView indexPathForSelectedRow] animated:YES];
    }
    
}
/*
// Modifications to design of sections
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {

    UIView *sectionView = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 22)] autorelease];
    sectionView.backgroundColor = [[UIColor grayColor] colorWithAlphaComponent:0.85];


    //Add label to view
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 22)];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.textColor = [UIColor whiteColor];
    
    //section Title
    if ([tableView isEqual:self.searchDisplayController.searchResultsTableView]){
        titleLabel.text = @"";
    } else {
        titleLabel.text = [self tableView:tableView titleForHeaderInSection:section];
    }
    
    NSLog(@"We got label: %@", titleLabel.text);
    
    
    [sectionView addSubview:titleLabel];
    [titleLabel release];

    
    return sectionView;

}
*/

- (NSDate *)dateAtBeginningOfDayForDate:(NSDate *)inputDate
{
    // Use the user's current calendar and time zone
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSTimeZone *timeZone = [NSTimeZone systemTimeZone];
    [calendar setTimeZone:timeZone];
    
    // Selectively convert the date components (year, month, day) of the input date
    NSDateComponents *dateComps = [calendar components:NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit fromDate:inputDate];
    
    // Set the time components manually
    [dateComps setHour:0];
    [dateComps setMinute:0];
    [dateComps setSecond:0];
    
    // Convert back
    NSDate *beginningOfDay = [calendar dateFromComponents:dateComps];
    return beginningOfDay;
}

- (NSDate *)dateByAddingYears:(NSInteger)numberOfYears toDate:(NSDate *)inputDate
{
    // Use the user's current calendar
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    NSDateComponents *dateComps = [[NSDateComponents alloc] init];
    [dateComps setYear:numberOfYears];
    
    NSDate *newDate = [calendar dateByAddingComponents:dateComps toDate:inputDate options:0];
    return newDate;
}




// For displaying an error if no data could be fetched..
-(IBAction)showMessage:(id)sender{

    // Stopp spinner, then display message
    [self.spinner stopAnimating];
    UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Feil"
                                                      message:@"Kunne ikke laste ned nyheter og program fra Studentuka :( "
                                                     delegate:self
                                            cancelButtonTitle:nil
                                            otherButtonTitles:@"Prøv på nytt",nil];
    [message show];
}

// Responding to buttons in error
-(void)alertView:(UIAlertView *) alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    if ([title isEqualToString:@"Prøv på nytt"]){
        printf("Fetching data once more..");
        numRetries++;
        if(numRetries > 10){
            [self showMessageAreYouSure:self];
            numRetries = 0;
        }
        else{
            [self viewDidLoad];
        }
    }
    else if ([title isEqualToString:@"Seriøst?"]){
        [self viewDidLoad];
    }
    
}

-(IBAction)showMessageAreYouSure:(id)sender{
    UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Seriøst?"
                                                      message:@"Sjekk internett-tilkoblingen din!"
                                                     delegate:self
                                            cancelButtonTitle:nil
                                            otherButtonTitles:@"Prøv på nytt",nil];
    [message show];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
