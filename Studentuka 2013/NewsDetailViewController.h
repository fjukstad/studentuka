//
//  NewsDetailViewController.h
//  Studentuka 2013
//
//  Created by Bjørn Fjukstad on 12/11/12.
//  Copyright (c) 2012 studentuka. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewsDetailViewController : UIViewController <UIWebViewDelegate, UIActionSheetDelegate>  {
    IBOutlet UIWebView *browser;
    IBOutlet UIBarButtonItem *button;
}

@property (nonatomic, retain) UIWebView *browser;
@property (strong, nonatomic) NSDictionary *newsStory;

- (IBAction)showActions:(id)sender;

@end

