//
//  AppDelegate.m
//  Studentuka 2013
//
//  Created by Bjørn Fjukstad on 12/9/12.
//  Copyright (c) 2012 studentuka. All rights reserved.
//

#import "AppDelegate.h"

@implementation AppDelegate

- (void)dealloc
{
    [_window release];
    [super dealloc];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
   
   /* [[UITabBarItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                       [UIFont fontWithName:@"Helvetica" size:10.0f], UITextAttributeFont,
                                                       [UIColor yellowColor], UITextAttributeTextColor,
                                                       nil] forState:UIControlStateNormal];*/
    CGFloat nRed=219.0/255.0;
    CGFloat nBlue=176.0/255.0;
    CGFloat nGreen=0.0/255.0;
    //UIColor *yellow=[[UIColor alloc]initWithRed:nRed green:nBlue blue:nGreen alpha:1.0];
    
    nRed=55.0/255.0;
    nBlue=106.0/255.0;
    nGreen=188.0/255.0;
    UIColor *blue=[[UIColor alloc]initWithRed:nRed green:nBlue blue:nGreen alpha:1.0];
    
    
    [[UITabBar appearance] setSelectedImageTintColor:blue];
    /*
    NSDictionary *navbarTitleTextAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                               yellow,UITextAttributeTextColor,
                                               [UIColor whiteColor], UITextAttributeTextShadowColor,
                                               [NSValue valueWithUIOffset:UIOffsetMake(-1, 0)], UITextAttributeTextShadowOffset, nil];
    
    [[UINavigationBar appearance] setTitleTextAttributes:navbarTitleTextAttributes];*/
    //[[UINavigationBar appearance] setSelectedImageTintColor:yellow];
    
    return YES;
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
