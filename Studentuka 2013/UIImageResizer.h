//
//  UIImageResizer.h
//  Studentuka 2013
//
//  Created by Bjørn Fjukstad on 1/17/13.
//  Copyright (c) 2013 studentuka. All rights reserved.
//

@interface UIImage (Resize)
- (UIImage*)scaleToSize:(CGSize)size;
@end