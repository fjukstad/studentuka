//
//  FirstViewController.m
//  Studentuka 2013
//
//  Created by Bjørn Fjukstad on 12/9/12.
//  Copyright (c) 2012 studentuka. All rights reserved.
//
#import "FirstViewController.h"
#import "NewsDetailViewController.h"


#define queue dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)
#define newsUrl [NSURL URLWithString:@"http://studentuka.no/api/get_recent_posts/"]

@interface FirstViewController ()

@end

@implementation FirstViewController {
    
    PullToRefreshView *pull;
    
}

@synthesize newsFeed;
@synthesize newsTable;
@synthesize spinner;

- (void)viewDidLoad
{
    [super viewDidLoad];

    pull = [[PullToRefreshView alloc] initWithScrollView:(UIScrollView *) self.newsTable];
    [pull setDelegate:self];
    [self.newsTable addSubview:pull];
    
    [self loadData];
}

- (void)pullToRefreshViewShouldRefresh:(PullToRefreshView *)view;
{
    [self loadData];
    [pull finishedLoading];
}

- (void)loadData
{
    // Spinner for indicating work
    self.spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    self.spinner.center = self.view.center;
    self.spinner.hidesWhenStopped = YES;
    [self.view addSubview:spinner];
    self.spinner.transform = CGAffineTransformMakeScale(1.25f, 1.25f);
    [self.spinner startAnimating];
    
    // Do any additional setup after loading the view, typically from a nib.
    
    // Fetch some news from Studentuka! 
    dispatch_async(queue, ^{
    //dispatch_after(popTime, queue, ^{
        
        // Only ok if performed in a background thread
        NSData* data = [NSData dataWithContentsOfURL:
                        newsUrl];
        // When data has been downloaded, the method fetchedData is
        // called. 
        [self performSelectorOnMainThread:@selector(fetchedData:)
                               withObject:data waitUntilDone:YES];
        
        

    });
    
}

/* Making the navbar dissappear */
- (void)viewWillAppear:(BOOL)animated {
    self.navigationController.navigationBar.hidden = YES;
   }

- (void)fetchedData:(NSData *)responseData {
    printf("FETCHED DATA\n");
    if(responseData == nil){
        [self showMessage:self];
    }
    else{
        //parse out the json data
        NSError* error;
        NSDictionary* json = [NSJSONSerialization
                              JSONObjectWithData:responseData //1
                              
                              options:kNilOptions
                              error:&error];
        printf("Got so far...\n");
        // Fetch some posts
        NSArray *latestPosts = [json objectForKey:@"posts"];
        NSMutableArray *tempArray = [[NSMutableArray alloc] init];
        
        
        NSLog(@"json: %@", latestPosts);
        
        // For all posts get their content
        
        for (int i = 0; i < [latestPosts count]; i++) {
            NSDictionary* content = [latestPosts objectAtIndex:i];
            
            NSArray *categories = [content objectForKey:@"categories"];
            NSLog(@"categories: %@", categories);
            
            NSDictionary *category = [categories objectAtIndex:0];
            NSNumber *catid = [category objectForKey:@"id"];
            
            // Skip any with category 11 = only for volunteers
            if([catid isEqualToNumber:[NSNumber numberWithInt:11]])
                continue;
            [tempArray addObject:content];
        }

        // Copy news into newsfeed array which can be accessed
        // when viewing the table .
        [self.newsFeed release];
        self.newsFeed = [[NSArray alloc] initWithArray:tempArray];
        
        // Stop spinner
        
        [self.spinner stopAnimating];
        
        // Force update of data in table
        [newsTable reloadData];
        
        // Don't need this anymore
        [tempArray release];
    }
}

// For displaying an error if no data could be fetched..
-(IBAction)showMessage:(id)sender{
    [self.spinner stopAnimating];
    UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Feil"
                                                      message:@"Kunne ikke laste ned nyheter og program fra Studentuka :( "
                                                     delegate:self
                                            cancelButtonTitle:nil
                                            otherButtonTitles:@"Prøv på nytt",nil];
    [message show];
}

// Responding to buttons in error
-(void)alertView:(UIAlertView *) alertView clickedButtonAtIndex:(NSInteger)buttonIndex{

    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    if ([title isEqualToString:@"Prøv på nytt"]){
        printf("Fetching data once more..");
        numRetries++;
        if(numRetries > 10){
            [self showMessageAreYouSure:self];
            numRetries = 0;
        }
        else{
            [self viewDidLoad];
        }
    }
    else if ([title isEqualToString:@"Seriøst?"]){
        [self viewDidLoad];
    }
    
}

-(IBAction)showMessageAreYouSure:(id)sender{
    UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Seriøst?"
                                                      message:@"Sjekk internett-tilkoblingen din!"
                                                     delegate:self
                                            cancelButtonTitle:nil
                                            otherButtonTitles:@"Prøv på nytt",nil];
    [message show]; 
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView  numberOfRowsInSection:(NSInteger)section {
    return [self.newsFeed count];
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];
        
        // Cell selection color
        CGFloat nRed=219.0/255.0;
        CGFloat nBlue=176.0/255.0;
        CGFloat nGreen=0.0/255.0;
        //UIColor *yellow=[[UIColor alloc]initWithRed:nRed green:nBlue blue:nGreen alpha:1.0];
        
        nRed=55.0/255.0;
        nBlue=106.0/255.0;
        nGreen=188.0/255.0;
        UIColor *blue=[[UIColor alloc]initWithRed:nRed green:nBlue blue:nGreen alpha:1.0];
        
        UIView *v = [[[UIView alloc] init] autorelease];
    	v.backgroundColor = blue;
    	cell.selectedBackgroundView = v;
    }
    
    // Get dict stored in news feed - the entire story with id, date and so on
    NSDictionary* content = [self.newsFeed objectAtIndex: [indexPath row]];
    
    /* Fetch info */ 
    NSString* post = [content objectForKey:@"content"];
    //NSNumber* postid = [content objectForKey:@"id"];
    NSString* title = [content objectForKey:@"title"];
    NSString* date = [content objectForKey:@"date"];
    
    cell.textLabel.text = title;
    
    // Convert date to a better format than website supplied
    NSDateFormatter *dateReader = [[NSDateFormatter alloc] init];
    NSDateFormatter *dateWriter = [[NSDateFormatter alloc] init];
    [dateReader setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [dateWriter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"nb_NO"]];
    [dateWriter setDateFormat:@"d. MMMM yyyy"];
    NSDate *nDate = [dateReader dateFromString:date];
    NSString *newDate = [dateWriter stringFromDate:nDate];

    NSLog(@"post: %@", post); 
    
    cell.detailTextLabel.text = newDate;
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    // Release what we don't need
    [dateReader release];
    [dateWriter release];
    
    return cell;
} 

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    printf("entered...\n");
    
    if ([segue.identifier isEqualToString:@"newsDetail"])
    {
        NewsDetailViewController *viewController = (NewsDetailViewController *)segue.destinationViewController;
        // [viewcontroller setNewsStory:c]
        
        NSIndexPath *indexPath = [newsTable indexPathForSelectedRow];
        viewController.newsStory = [newsFeed objectAtIndex:indexPath.row];
        
        // Deselect so that it isn't selected when we return
        [newsTable deselectRowAtIndexPath:[newsTable indexPathForSelectedRow] animated:YES];
    }

}
/*
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    // Modify cell appearance - coloring etc.
    
    if (indexPath.row % 2)
    {
        //[ cell setBackgroundColor:[UIColor colorWithRed:.8 green:.8 blue:1 alpha:1]];
         
    }
    else [cell setBackgroundColor:[UIColor whiteColor]];
     
}
*/
 
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"Clicked row: %d \n", indexPath.row);
    
    
   [self performSegueWithIdentifier:@"newsDetail" sender:self];


}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)dealloc{
    [newsFeed release];
    [super dealloc]; 
}

@end
