Studentuka
==========

iPhone app for *Studentuka 2013* in Tromsø. Note that the source code has not
been modified since it was published and left alone. It can be found on the
App Store [here] [1].

How to compile and run
------

Open _Studentuka 2013.xcodeproj_ using Xcode. Hopefully it will compile and run
without any hassle. Note that if you try to run the app using the iPad
simulator, it will crash. This is an app only intended for iPhones.  

[1]: https://itunes.apple.com/no/app/studentuka/id591203035?l=en&mt=8 "here"
